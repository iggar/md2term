% md2term(1) | General use manual

## NAME

md2term - Markdown parser for highlighting and colors on the terminal

## DESCRIPTION

`md2term` was created to be a markdown parser for short text (under 30 lines) which could be used in presentations on the terminal (text-based slides) and at the same time be correctly visualized by other web-based parsers (e.g. Github, GitLab). This script can also be used sufficiently well to visualize any  markdown text, as long as its limitations are tolerated.

## SYNOPSIS

md2term \[OPTIONS\] -f FILE

## OPTIONS


  -b CHARACTER\ \ \ List marker character.

  -c\ \ \ \ \ \ \ \ \ \ \ \ \ Clear terminal before showing text.

  -i SPACES\ \ \ \ \ Indentation for lists, code and mentions in spaces.

  -k VALUE\ \ \ \ \ \ \ Keep hashes on headings:

\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 0=Discard all 1=Keep all 2=Only heading 1

  -m SPACES\ \ \ \ \ Left margin padding in spaces.

  -p\ \ \ \ \ \ \ \ \ \ \ \ \ Load the result on a pager (less).

  -s FILE\ \ \ \ \ Alternative color theme.

  -t LINES\ \ \ \ \ \ Top padding in lines.

  -h\ \ \ \ \ \ \ \ \ \ \ \ \ Print this help.

## VALID MARKUPS

| Markup | Result |
|---|---|
| `**TEXT**` or `__TEXT__` | BOLD |
| `*TEXT*` or `*TEXT*` | ITALIC |
| `___TEXT___` or `***TEXT***` | BOLD AND ITALIC |
| `~~TEXT~~` | STRIKETHROUGH |
| `<u>TEXT</u>` | UNDERLINE |
| ``` `TEXT` ``` | INLINE CODE |
| `[LABEL](URL)` | LABEL |

## BLOCKS

Code block markings must be done at the beginning of the line!

### Code block

~~~
```
CODE BLOCK
```
~~~

### Unordered lists

```
- Item 1
- Item 2
- Item 3
```

### Ordered lists

```
1. Item 1
1. Item 2
1. Item 3
```

### Citations/notes

```
> Citation or note text.
```

### Force line break

```
Paragraphs, list items and citations \
can break lines at the desired point \
by using backslashes.
```

## LIMITATIONS

- Nested lists (sub-items) are not interpreted correctly.
- Long lines can break in the middle of words, but it is possible to work around that problem by specifying where they should occur on the source text (it is a valid markup on both GitLab and GitHub).
- Using both options `-c` and `-p` together might lead to buffer re-rendering issues on `less`.
- Links are just visual representations using the markup label, not the URL. If you prefer the URL, it can be used as a label or be written as text without markup (most of the terminals interpret URLs as links).

## LICENSE

Copyright (C) 2022 Blau Araujo <blau@debxp.org>

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html> \
This is a free software: you are free to modify it and redistribute it. \
This program comes with ABSOLUTELY NO WARRANTY, to the extent permitted by \
applicable law. 

Developed by Blau Araujo and Romeu Alfa
