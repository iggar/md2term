PREFIX ?= /usr
MANDIR ?= $(PREFIX)/share/man
SHAREDIR ?= $(PREFIX)/share

all:
	echo Run \'make install\' to install debfetch.

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	mkdir -p $(DESTDIR)$(SHAREDIR)/md2term/themes
	cp -p md2term $(DESTDIR)$(PREFIX)/bin/md2term
	cp -p md2term.1 $(DESTDIR)$(MANDIR)/man1
	cp -p ./themes/dark-theme $(DESTDIR)$(SHAREDIR)/md2term/themes

uninstall:
	rm $(DESTDIR)$(PREFIX)/bin/md2term
	rm $(DESTDIR)$(MANDIR)/man1/md2term.1*
	rm -rf $(DESTDIR)$(SHAREDIR)/md2term
